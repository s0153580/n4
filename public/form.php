<!DOCTYPE html>
<html lang="ru">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Project 4</title>
  <!-- CSS only -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
    integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <!-- JS, Popper.js, and jQuery -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
    integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
    integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
    crossorigin="anonymous"></script>
  <!-- Main CSS -->
  <link rel="stylesheet" href="style.css">
</head>

<body class="text-dark">
  <div class="container-fluid p-0 w-100 overflow-hidden">
    <header class="row d-flex flex-row justify-content-center">
      <div
        class="d-flex flex-row align-items-center justify-content-around justify-content-md-start col-md-9 px-md-4 h-100">
        <img class="mr-md-2 border border-white rounded-circle" id="img" src="logo.png" alt="LOGO">
        <div class="text-body" id="name">Project 4</div>
      </div>
    </header>

    <div class="row d-flex flex-row justify-content-center">
      <div class="d-flex col-md-9 p-0 ">
        <div class="items d-flex flex-column align-content-center">

          <div id="form" class="align-content-center">

            <h2>Форма</h2>

            <?php
            if (!empty($messages)) {
              print('<div id="messages">');
              foreach ($messages as $message) {
                print($message);
                }
              print('</div>');
            }
            ?>

            <form action="" method="POST">

              <label>
                Имя:<br />
                <input name="field-name" <?php if ($errors['field-name']) {print "class='error'" ;} ?> value="<?php print $values['field-name']; ?>" />
              </label><br />

              <label>
                E-mail:<br />
                <input name="field-email" <?php if ($errors['field-email']) {print 'class="error"' ;} ?> value="<?php print $values['field-email']; ?>"  />
              </label><br />

              <label>
                ДР:<br />
                <input name="field-date" <?php if ($errors['field-date']) {print 'class="error"' ;} ?> value="<?php print $values['field-date']; ?>" />
              </label><br />

              Пол:<br />
              <label><input type="radio" name="radio-group-1" <?php if ($errors['radio-group-1']) {print 'class="error"' ;} if($values['radio-group-1']=="М"){print "checked='checked'";}?> value="М" />
                М</label>
              <label><input type="radio" name="radio-group-1" <?php if ($errors['radio-group-1']) {print 'class="error"' ;} if($values['radio-group-1']=="Ж"){print "checked='checked'";}?> value="Ж" />
                Ж</label><br />

              Кол-во конечностей:<br />
              <label><input type="radio" name="radio-group-2" <?php if ($errors['radio-group-2']) {print 'class="error"' ;} if($values['radio-group-2']=="4"){print "checked='checked'";}?> value="4" />
                4</label>
              <label><input type="radio" name="radio-group-2" <?php if ($errors['radio-group-2']) {print 'class="error"' ;} if($values['radio-group-2']=="8"){print "checked='checked'";}?> value="8" />
                8</label>
              <label><input type="radio" name="radio-group-2" <?php if ($errors['radio-group-2']) {print 'class="error"' ;} if($values['radio-group-2']=="22"){print "checked='checked'";}?> value="22" />
                22</label><br />

              <label>
                Сверхспособности:
                <br />
                <select name="field-name-4" multiple="multiple">
                  <option value="Бессмертие" <?php if($values['field-name-4']=="Бессмертие"){print "selected='selected'";}?>>Бессмертие</option>
                  <option value="Прохождение сквозь стены" <?php if($values['field-name-4']=="Прохождение сквозь стены"){print "selected='selected'";}?>>Прохождение сквозь стены</option>
                  <option value="Левитация" <?php if($values['field-name-4']=="Левитация"){print "selected='selected'";}?>>Левитация</option>
                </select>
              </label><br />

              <label>
                Биография:<br />
                <textarea name="field-name-2" <?php if ($errors['field-name-2']) {print 'class="error"' ;} ?>><?php print $values['field-name-2']; ?></textarea> 
              </label><br />

              Ознакомлен:<br />
              <label><input type="checkbox" name="check-1" <?php  if($values['check-1']=="1"){print "checked='checked'";}?> value="1" />
                Соглашаюсь со всем</label><br /> 

              <input type="submit" value="Отправить" />

            </form>
            <p><a id="bottom"></a></p>
          </div>

        </div>
      </div>
    </div>

    <footer class="row d-flex flex-row justify-content-center h-md-75">
      <div class="d-flex flex-row align-items-center col-md-9">
        <div class="text-body">(с) Виктор Глоба 2021</div>
      </div>
    </footer>
  </div>
</body>

</html>
